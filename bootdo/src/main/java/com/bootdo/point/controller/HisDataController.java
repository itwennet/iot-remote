package com.bootdo.point.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.point.domain.HisDataDO;
import com.bootdo.point.service.HisDataService;

/**
 * 实时数据
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:42:21
 */
 
@Controller
@RequestMapping("/point/hisData")
public class HisDataController {
	@Autowired
	private HisDataService hisDataService;
	
	@GetMapping()
	@RequiresPermissions("point:hisData:hisData")
	String HisData(){
	    return "point/hisData/hisData";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("point:hisData:hisData")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<HisDataDO> hisDataList = hisDataService.list(query);
		int total = hisDataService.count(query);
		PageUtils pageUtils = new PageUtils(hisDataList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("point:hisData:add")
	String add(){
	    return "point/hisData/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("point:hisData:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		HisDataDO hisData = hisDataService.get(id);
		model.addAttribute("hisData", hisData);
	    return "point/hisData/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("point:hisData:add")
	public R save( HisDataDO hisData){
		if(hisDataService.save(hisData)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("point:hisData:edit")
	public R update( HisDataDO hisData){
		hisDataService.update(hisData);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("point:hisData:remove")
	public R remove( Integer id){
		if(hisDataService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("point:hisData:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		hisDataService.batchRemove(ids);
		return R.ok();
	}
	
}
