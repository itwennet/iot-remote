package com.bootdo.point.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 实时数据
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:42:21
 */
public class HisDataDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//设备ID
	private String deviceId;
	//温度
	private Integer temperature;
	//湿度
	private Integer humidity;
	//创建日期
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：设备ID
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * 获取：设备ID
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * 设置：温度
	 */
	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}
	/**
	 * 获取：温度
	 */
	public Integer getTemperature() {
		return temperature;
	}
	/**
	 * 设置：湿度
	 */
	public void setHumidity(Integer humidity) {
		this.humidity = humidity;
	}
	/**
	 * 获取：湿度
	 */
	public Integer getHumidity() {
		return humidity;
	}
	/**
	 * 设置：创建日期
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建日期
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
